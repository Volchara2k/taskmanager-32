package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for deleteAllProjectsResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="deleteAllProjectsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deletedProjects" type="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}projectDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteAllProjectsResponse", propOrder = {
        "deletedProjects"
})
public class DeleteAllProjectsResponse {

    protected List<ProjectDTO> deletedProjects;

    /**
     * Gets the value of the deletedProjects property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the deletedProjects property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDeletedProjects().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProjectDTO }
     */
    public List<ProjectDTO> getDeletedProjects() {
        if (deletedProjects == null) {
            deletedProjects = new ArrayList<ProjectDTO>();
        }
        return this.deletedProjects;
    }

}
