package ru.renessans.jvschool.volkov.task.manager.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public class ProjectCreateListener extends AbstractProjectListener {

    @NotNull
    private static final String CMD_PROJECT_CREATE = "project-create";

    @NotNull
    private static final String DESC_PROJECT_CREATE = "добавить новый проект";

    @NotNull
    private static final String NOTIFY_PROJECT_CREATE =
            "Происходит попытка инициализации создания проекта. \n" +
                    "Для создания проекта введите его заголовок и описание. ";

    @Autowired
    public ProjectCreateListener(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(projectEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_PROJECT_CREATE;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_PROJECT_CREATE;
    }

    @Async
    @Override
    @EventListener(condition = "@projectCreateListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        ViewUtil.print(NOTIFY_PROJECT_CREATE);
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @Nullable final ProjectDTO Create = super.projectEndpoint.addProject(current, title, description);
        ViewUtil.print(Create);
    }

}