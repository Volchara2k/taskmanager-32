package ru.renessans.jvschool.volkov.task.manager.listener.admin.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.admin.data.AbstractAdminDataListener;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public class DataXmlImportListener extends AbstractAdminDataListener {

    @NotNull
    private static final String CMD_XML_IMPORT = "data-xml-import";

    @NotNull
    private static final String DESC_XML_IMPORT = "импортировать домен из xml вида";

    @NotNull
    private static final String NOTIFY_XML_IMPORT = "Происходит процесс загрузки домена из xml вида...";

    @Autowired
    public DataXmlImportListener(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_XML_IMPORT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_XML_IMPORT;
    }

    @Async
    @Override
    @EventListener(condition = "@dataXmlImportListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        @NotNull final DomainDTO importData = super.adminDataInterChangeEndpoint.importDataXml(current);
        ViewUtil.print(NOTIFY_XML_IMPORT);
        ViewUtil.print(importData.getUsers());
        ViewUtil.print(importData.getTasks());
        ViewUtil.print(importData.getProjects());
    }

}