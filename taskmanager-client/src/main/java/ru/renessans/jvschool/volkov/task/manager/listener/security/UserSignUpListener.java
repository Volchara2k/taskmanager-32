package ru.renessans.jvschool.volkov.task.manager.listener.security;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AuthenticationEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public class UserSignUpListener extends AbstractListener {

    @NotNull
    private static final String CMD_SIGN_UP = "sign-up";

    @NotNull
    private static final String DESC_SIGN_UP = "зарегистрироваться в системе";

    @NotNull
    private static final String NOTIFY_SIGN_UP =
            "Происходит попытка инициализации регистрации пользователя. \n" +
                    "Для регистрации пользователя в системе введите логин и пароль: ";

    @NotNull
    private final AuthenticationEndpoint authenticationEndpoint;

    @NotNull
    private final ICurrentSessionService currentSessionService;

    @Autowired
    public UserSignUpListener(
            @NotNull final AuthenticationEndpoint authenticationEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        this.authenticationEndpoint = authenticationEndpoint;
        this.currentSessionService = currentSessionService;
    }

    @NotNull
    @Override
    public String command() {
        return CMD_SIGN_UP;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_SIGN_UP;
    }

    @Async
    @Override
    @EventListener(condition = "@userSignUpListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        ViewUtil.print(NOTIFY_SIGN_UP);
        @NotNull final String login = ViewUtil.getLine();
        @NotNull final String password = ViewUtil.getLine();
        @Nullable final SessionDTO current = this.currentSessionService.getSession();
        @Nullable final UserLimitedDTO create = this.authenticationEndpoint.signUpUser(current, login, password);
        ViewUtil.print(create);
    }

}