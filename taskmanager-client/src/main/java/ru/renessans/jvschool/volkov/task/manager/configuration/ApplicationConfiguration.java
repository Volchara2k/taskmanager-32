package ru.renessans.jvschool.volkov.task.manager.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import ru.renessans.jvschool.volkov.task.manager.endpoint.*;

import java.util.concurrent.Executor;

@EnableAsync
@Configuration
@ComponentScan("ru.renessans.jvschool.volkov.task.manager")
public class ApplicationConfiguration {

    @Bean
    @NotNull
    public Executor executor() {
        @NotNull final ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(2);
        taskExecutor.setMaxPoolSize(2);
        taskExecutor.setQueueCapacity(500);
        taskExecutor.setThreadNamePrefix("Terminal Task-");
        taskExecutor.initialize();
        return taskExecutor;
    }

    @Bean
    @NotNull
    public AuthenticationEndpointService authenticationEndpointService() {
        return new AuthenticationEndpointService();
    }

    @Bean
    @NotNull
    public AdminEndpointService adminEndpointService() {
        return new AdminEndpointService();
    }

    @Bean
    @NotNull
    public AdminDataInterChangeEndpointService adminDataInterChangeEndpointService() {
        return new AdminDataInterChangeEndpointService();
    }

    @Bean
    @NotNull
    public SessionEndpointService sessionEndpointService() {
        return new SessionEndpointService();
    }

    @Bean
    @NotNull
    public UserEndpointService userEndpointService() {
        return new UserEndpointService();
    }

    @Bean
    @NotNull
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @Bean
    @NotNull
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @Bean
    @NotNull
    public AuthenticationEndpoint authenticationEndpoint(
            @NotNull final AuthenticationEndpointService authenticationEndpointService
    ) {
        return authenticationEndpointService.getAuthenticationEndpointPort();
    }

    @Bean
    @NotNull
    public AdminEndpoint adminEndpoint(
            @NotNull final AdminEndpointService adminEndpointService
    ) {
        return adminEndpointService.getAdminEndpointPort();
    }

    @Bean
    @NotNull
    public AdminDataInterChangeEndpoint adminDataInterChangeEndpoint(
            @NotNull final AdminDataInterChangeEndpointService adminDataInterChangeEndpointService
    ) {
        return adminDataInterChangeEndpointService.getAdminDataInterChangeEndpointPort();
    }

    @Bean
    @NotNull
    public SessionEndpoint sessionEndpoint(
            @NotNull final SessionEndpointService sessionEndpointService
    ) {
        return sessionEndpointService.getSessionEndpointPort();
    }

    @Bean
    @NotNull
    public UserEndpoint userEndpoint(
            @NotNull final UserEndpointService userEndpointService
    ) {
        return userEndpointService.getUserEndpointPort();
    }

    @Bean
    @NotNull
    public ProjectEndpoint projectEndpoint(
            @NotNull final ProjectEndpointService projectEndpointService
    ) {
        return projectEndpointService.getProjectEndpointPort();
    }

    @Bean
    @NotNull
    public TaskEndpoint taskEndpoint(
            @NotNull final TaskEndpointService taskEndpointService
    ) {
        return taskEndpointService.getTaskEndpointPort();
    }

}