package ru.renessans.jvschool.volkov.task.manager.listener.admin.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.admin.data.AbstractAdminDataListener;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public class DataJsonClearListener extends AbstractAdminDataListener {

    @NotNull
    private static final String CMD_JSON_CLEAR = "data-json-clear";

    @NotNull
    private static final String DESC_JSON_CLEAR = "очистить json данные";

    @NotNull
    private static final String NOTIFY_JSON_CLEAR = "Происходит процесс очищения json данных...";

    @Autowired
    public DataJsonClearListener(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_JSON_CLEAR;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_JSON_CLEAR;
    }

    @Async
    @Override
    @EventListener(condition = "@dataJsonClearListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        @Nullable final SessionDTO current = super.currentSessionService.getSession();
        final boolean close = super.adminDataInterChangeEndpoint.dataJsonClear(current);
        ViewUtil.print(NOTIFY_JSON_CLEAR);
        ViewUtil.print(close);
    }

}