package ru.renessans.jvschool.volkov.task.manager.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;

import java.util.Collection;

@Service
public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository iCommandRepository;

    @Lazy
    @Autowired
    public CommandService(
            @NotNull final ICommandRepository commandRepository
    ) {
        this.iCommandRepository = commandRepository;
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getAllCommands() {
        return this.iCommandRepository.getAllCommands();
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getAllTerminalCommands() {
        return this.iCommandRepository.getAllTerminalCommands();
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getAllArgumentCommands() {
        return this.iCommandRepository.getAllArgumentCommands();
    }

}