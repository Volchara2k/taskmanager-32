package ru.renessans.jvschool.volkov.task.manager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractListener> getAllCommands();

    @NotNull
    Collection<AbstractListener> getAllTerminalCommands();

    @NotNull
    Collection<AbstractListener> getAllArgumentCommands();

}