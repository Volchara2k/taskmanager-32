package ru.renessans.jvschool.volkov.task.manager.listener.admin;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminEndpoint;

public abstract class AbstractAdminListener extends AbstractListener {

    @NotNull
    protected final AdminEndpoint adminEndpoint;

    @NotNull
    protected final ICurrentSessionService currentSessionService;

    @Autowired
    protected AbstractAdminListener(
            @NotNull final AdminEndpoint adminEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        this.adminEndpoint = adminEndpoint;
        this.currentSessionService = currentSessionService;
    }

}