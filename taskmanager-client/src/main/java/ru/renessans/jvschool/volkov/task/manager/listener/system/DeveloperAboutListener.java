package ru.renessans.jvschool.volkov.task.manager.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
@SuppressWarnings("unused")
public class DeveloperAboutListener extends AbstractListener {

    @NotNull
    private static final String CMD_ABOUT = "about";

    @NotNull
    private static final String ARG_ABOUT = "-a";

    @NotNull
    private static final String DESC_ABOUT = "вывод информации о разработчике";

    @NotNull
    private static final String NOTIFY_ABOUT = "%s - разработчик; \n%s - почта.";

    @NotNull
    private static final String DEVELOPER = "Valery Volkov";

    @NotNull
    private static final String DEVELOPER_MAIL = "volkov.valery2013@yandex.ru";

    @NotNull
    @Override
    public String command() {
        return CMD_ABOUT;
    }

    @NotNull
    @Override
    public String argument() {
        return ARG_ABOUT;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_ABOUT;
    }

    @Async
    @Override
    @EventListener(condition = "@developerAboutListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        ViewUtil.print(String.format(NOTIFY_ABOUT, DEVELOPER, DEVELOPER_MAIL));
    }

}