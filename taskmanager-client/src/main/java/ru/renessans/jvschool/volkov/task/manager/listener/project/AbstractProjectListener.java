package ru.renessans.jvschool.volkov.task.manager.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;

public abstract class AbstractProjectListener extends AbstractListener {

    @NotNull
    protected final ProjectEndpoint projectEndpoint;

    @NotNull
    protected final ICurrentSessionService currentSessionService;

    @Autowired
    protected AbstractProjectListener(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        this.projectEndpoint = projectEndpoint;
        this.currentSessionService = currentSessionService;
    }

}