package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;

import java.util.Collection;

public interface ICommandService {

    @Nullable
    Collection<AbstractListener> getAllCommands();

    @Nullable
    Collection<AbstractListener> getAllTerminalCommands();

    @Nullable
    Collection<AbstractListener> getAllArgumentCommands();

}