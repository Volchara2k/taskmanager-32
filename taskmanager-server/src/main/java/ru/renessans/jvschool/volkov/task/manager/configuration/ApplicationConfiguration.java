package ru.renessans.jvschool.volkov.task.manager.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEntityManagerFactoryService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

@Configuration
@ComponentScan("ru.renessans.jvschool.volkov.task.manager")
public class ApplicationConfiguration {

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory(
            @NotNull final IEntityManagerFactoryService entityManagerFactoryService
    ) {
        return entityManagerFactoryService.buildFactory();
    }

    @Bean
    @NotNull
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public EntityManager entityManager(
           @NotNull final EntityManagerFactory entityManagerFactory
    ) {
        return entityManagerFactory.createEntityManager();
    }

}