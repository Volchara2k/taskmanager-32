package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDataInterChangeService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDomainService;
import ru.renessans.jvschool.volkov.task.manager.dto.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.util.DataMarshalizerUtil;
import ru.renessans.jvschool.volkov.task.manager.util.DataSerializerUtil;
import ru.renessans.jvschool.volkov.task.manager.util.FileUtil;

@Service
public final class DataInterChangeService implements IDataInterChangeService {

    @NotNull
    private final IConfigurationService configService;

    @NotNull
    private final IDomainService domainService;

    @Autowired
    public DataInterChangeService(
            @NotNull final IConfigurationService configurationService,
            @NotNull final IDomainService domainService
    ) {
        this.configService = configurationService;
        this.domainService = domainService;
    }

    @Override
    public boolean dataBinClear() {
        @NotNull final String pathname = this.configService.getBinPathname();
        return FileUtil.delete(pathname);
    }

    @Override
    public boolean dataBase64Clear() {
        @NotNull final String pathname = this.configService.getBase64Pathname();
        return FileUtil.delete(pathname);
    }

    @Override
    public boolean dataJsonClear() {
        @NotNull final String pathname = this.configService.getJsonPathname();
        return FileUtil.delete(pathname);
    }

    @Override
    public boolean dataXmlClear() {
        @NotNull final String pathname = this.configService.getXmlPathname();
        return FileUtil.delete(pathname);
    }

    @Override
    public boolean dataYamlClear() {
        @NotNull final String pathname = this.configService.getYamlPathname();
        return FileUtil.delete(pathname);
    }

    @NotNull
    @SneakyThrows
    @Override
    public DomainDTO exportDataBin() {
        @NotNull final DomainDTO domain = new DomainDTO();
        this.domainService.dataExport(domain);
        @NotNull final String pathname = this.configService.getBinPathname();
        DataSerializerUtil.writeToBin(domain, pathname);
        return domain;
    }

    @NotNull
    @SneakyThrows
    @Override
    public DomainDTO exportDataBase64() {
        @NotNull final DomainDTO domain = new DomainDTO();
        this.domainService.dataExport(domain);
        @NotNull final String pathname = this.configService.getBase64Pathname();
        DataSerializerUtil.writeToBase64(domain, pathname);
        return domain;
    }

    @NotNull
    @Override
    public DomainDTO exportDataJson() {
        @NotNull final DomainDTO domain = new DomainDTO();
        this.domainService.dataExport(domain);
        @NotNull final String pathname = this.configService.getJsonPathname();
        DataMarshalizerUtil.writeToJson(domain, pathname);
        return domain;
    }

    @NotNull
    @Override
    public DomainDTO exportDataXml() {
        @NotNull final DomainDTO domain = new DomainDTO();
        this.domainService.dataExport(domain);
        @NotNull final String pathname = this.configService.getXmlPathname();
        DataMarshalizerUtil.writeToXml(domain, pathname);
        return domain;
    }

    @NotNull
    @SneakyThrows
    @Override
    public DomainDTO exportDataYaml() {
        @NotNull final DomainDTO domain = new DomainDTO();
        this.domainService.dataExport(domain);
        @NotNull final String pathname = this.configService.getYamlPathname();
        DataMarshalizerUtil.writeToYaml(domain, pathname);
        return domain;
    }

    @NotNull
    @SneakyThrows
    @Override
    public DomainDTO importDataBin() {
        @NotNull final String pathname = this.configService.getBinPathname();
        @NotNull final DomainDTO domain = DataSerializerUtil.readFromBin(pathname, DomainDTO.class);
        this.domainService.dataImport(domain);
        return domain;
    }

    @NotNull
    @SneakyThrows
    @Override
    public DomainDTO importDataBase64() {
        @NotNull final String pathname = this.configService.getBase64Pathname();
        @NotNull final DomainDTO domain = DataSerializerUtil.readFromBase64(pathname, DomainDTO.class);
        this.domainService.dataImport(domain);
        return domain;
    }

    @NotNull
    @Override
    public DomainDTO importDataJson() {
        @NotNull final String pathname = this.configService.getJsonPathname();
        @NotNull final DomainDTO domain = DataMarshalizerUtil.readFromJson(pathname, DomainDTO.class);
        this.domainService.dataImport(domain);
        return domain;
    }

    @NotNull
    @Override
    public DomainDTO importDataXml() {
        @NotNull final String pathname = this.configService.getXmlPathname();
        @NotNull final DomainDTO domain = DataMarshalizerUtil.readFromXml(pathname, DomainDTO.class);
        this.domainService.dataImport(domain);
        return domain;
    }

    @NotNull
    @Override
    public DomainDTO importDataYaml() {
        @NotNull final String pathname = this.configService.getYamlPathname();
        @NotNull final DomainDTO domain = DataMarshalizerUtil.readFromYaml(pathname, DomainDTO.class);
        this.domainService.dataImport(domain);
        return domain;
    }

}