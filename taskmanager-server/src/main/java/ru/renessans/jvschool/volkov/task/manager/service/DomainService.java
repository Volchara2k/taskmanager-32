package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IUserUnlimitedAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.file.InvalidDomainException;

import java.util.Objects;
import java.util.stream.Collectors;

@Service
public final class DomainService implements IDomainService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskUserService taskService;

    @NotNull
    private final IProjectUserService projectService;

    @NotNull
    private final IUserUnlimitedAdapterService userAdapter;

    @NotNull
    private final ITaskAdapterService taskAdapter;

    @NotNull
    private final IProjectAdapterService projectAdapter;

    public DomainService(
            @NotNull final IUserService userService,
            @NotNull final ITaskUserService taskUserService,
            @NotNull final IProjectUserService projectUserService,
            @NotNull final IUserUnlimitedAdapterService userUnlimitedAdapterService,
            @NotNull final ITaskAdapterService taskAdapterService,
            @NotNull final IProjectAdapterService projectAdapterService
    ) {
        this.userService = userService;
        this.taskService = taskUserService;
        this.projectService = projectUserService;
        this.userAdapter = userUnlimitedAdapterService;
        this.taskAdapter = taskAdapterService;
        this.projectAdapter = projectAdapterService;
    }

    @SneakyThrows
    @Override
    public DomainDTO dataImport(@Nullable final DomainDTO domain) {
        if (Objects.isNull(domain)) throw new InvalidDomainException();
        this.userService.setAllRecords(
                domain.getUsers()
                        .stream()
                        .map(this.userAdapter::toModel)
                        .collect(Collectors.toList())
        );

        this.taskService.setAllRecords(
                domain.getTasks()
                        .stream()
                        .map(this.taskAdapter::toModel)
                        .collect(Collectors.toList())
        );

        this.projectService.setAllRecords(
                domain.getProjects()
                        .stream()
                        .map(this.projectAdapter::toModel)
                        .collect(Collectors.toList())
        );
        return domain;
    }

    @SneakyThrows
    @Override
    public DomainDTO dataExport(@Nullable final DomainDTO domain) {
        if (Objects.isNull(domain)) throw new InvalidDomainException();
        domain.setUsers(
                this.userService.getAllRecords()
                        .stream()
                        .map(this.userAdapter::toDTO)
                        .collect(Collectors.toList())
        );
        domain.setTasks(
                this.taskService.getAllRecords()
                        .stream()
                        .map(this.taskAdapter::toDTO)
                        .collect(Collectors.toList())
        );
        domain.setProjects(
                this.projectService.getAllRecords()
                        .stream()
                        .map(this.projectAdapter::toDTO)
                        .collect(Collectors.toList())
        );
        return domain;
    }

}