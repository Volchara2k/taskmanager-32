package ru.renessans.jvschool.volkov.task.manager.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEntityManagerFactoryService;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Service
public final class EntityManagerFactoryService implements IEntityManagerFactoryService {

    @NotNull
    private final IConfigurationService configService;

    @Autowired
    public EntityManagerFactoryService(@NotNull final IConfigurationService configurationService) {
        this.configService = configurationService;
    }

    @NotNull
    @Override
    public EntityManagerFactory buildFactory() {
        @NotNull final StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
        @NotNull final Map<String, String> settings = getSettings();
        serviceRegistryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry serviceRegistry = serviceRegistryBuilder.build();

        @NotNull final MetadataSources metadataSources = new MetadataSources(serviceRegistry);
        metadataSources.addAnnotatedClass(Task.class);
        metadataSources.addAnnotatedClass(Project.class);
        metadataSources.addAnnotatedClass(User.class);
        metadataSources.addAnnotatedClass(Session.class);

        @NotNull final Metadata metadata = metadataSources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @NotNull
    private Map<String, String> getSettings() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, this.configService.getDriverJDBC());
        settings.put(Environment.URL, this.configService.getUrlJDBC());
        settings.put(Environment.USER, this.configService.getDatabaseLogin());
        settings.put(Environment.PASS, this.configService.getDatabasePassword());
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");
        settings.put(Environment.FORMAT_SQL, "true");
        return settings;
    }

}