package ru.renessans.jvschool.volkov.task.manager.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.IEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.util.EndpointUtil;

import javax.annotation.PostConstruct;
import java.util.Collection;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private ITaskUserService taskUserService;

    @NotNull
    @Autowired
    private IProjectUserService projectUserService;

    @NotNull
    @Autowired
    private IConfigurationService configurationService;

    @NotNull
    @Autowired
    private Collection<IEndpoint> endpoints;

    public void run() {
        recordDemoData();
    }

    private void recordDemoData() {
        @NotNull final Collection<User> users = this.userService.initialDemoData();
        this.taskUserService.initialDemoData(users);
        this.projectUserService.initialDemoData(users);
    }

    @PostConstruct
    private void publishWebServices() {
        @Nullable final String host = this.configurationService.getServerHost();
        @Nullable final Integer port = this.configurationService.getServerPort();
        EndpointUtil.publish(this.endpoints, host, port);
    }

}