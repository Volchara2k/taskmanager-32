package ru.renessans.jvschool.volkov.task.manager.api.service;

import ru.renessans.jvschool.volkov.task.manager.api.repository.IProjectUserRepository;
import ru.renessans.jvschool.volkov.task.manager.model.Project;

public interface IProjectUserService extends IOwnerUserService<Project, IProjectUserRepository> {
}