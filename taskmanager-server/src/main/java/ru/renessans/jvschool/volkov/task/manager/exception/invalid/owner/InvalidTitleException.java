package ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidTitleException extends AbstractException {

    @NotNull
    private static final String EMPTY_TITLE = "Ошибка! Параметр \"заголовок\" является пустым или null!\n";

    public InvalidTitleException() {
        super(EMPTY_TITLE);
    }

}